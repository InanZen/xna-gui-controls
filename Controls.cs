﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace XNA_GUI_Controls
{
    public enum TextAlignment
    {
        Center = 0,
        Left = 1,
        Right = 2
    }

    public class TextArea : Control
    {
        List<String> Lines;
        Vector2[] TextPos;
        SpriteFont font;
        String _text;
        TextAlignment align;
        public TextAlignment Alignment
        {
            get { return align; }
            set
            {
                align = value;
                ProcessString();
            }
        }

        /// <summary>
        /// Get and Set the Text of TextArea
        /// </summary>
        public String Text
        {
            get { return _text; }
            set
            {
                _text = value;
                ProcessString();
            }
        }       
        public SpriteFont Font
        {
            get { return font; }
            set
            {
                font = value;
                ProcessString();
            }
        }

        public TextArea(String name, Rectangle dest, String text, SpriteFont font) : base (name, dest)
        {
            Color = Color.Black;
            this.font = font;
            Text = text;
            base.PositionChanged += OnPosChange;
        }
        void ProcessString()
        {
            if (Font != null && _text != null)
            {
                Lines = new List<String>();
                var split = _text.Split(' ');
                Vector2 spaceSize = Font.MeasureString(" ");
                float h = Font.MeasureString("|").Y;
                float curSize = 0f;
                int startIndex = 0;
                for (int i = 0; i < split.Length; i++)
                {
                    Vector2 strSize = Font.MeasureString(split[i]);
                    curSize += strSize.X;
                    if (i != 0)
                        curSize += spaceSize.X;
                    if (curSize > Position.Width)
                    {
                        Lines.Add(String.Join(" ", split, startIndex, i - startIndex));
                        startIndex = i;
                        curSize = strSize.X;
                    }
                    if (i == split.Length - 1)
                        Lines.Add(String.Join(" ", split, startIndex, split.Length - startIndex));
                }
                TextPos = new Vector2[Lines.Count];
                int height = (int)(font.LineSpacing * Lines.Count);
                int startY = Position.Center.Y - height / 2;
                for (int i = 0; i < TextPos.Length; i++)
                {
                    Vector2 textSize = Font.MeasureString(Lines[i]);
                    
                    float x = Position.Center.X - textSize.X / 2;
                    if (align == TextAlignment.Left)
                        x = Position.X;
                    else if (align == TextAlignment.Right)
                        x = Position.Right - textSize.X;
                    TextPos[i] = new Vector2(x, startY + font.LineSpacing * i);
                }
            }
        }
        void OnPosChange(object sender, EventArgs e)
        {
            if (Font != null && _text != null)
            {
                ProcessString();
            }
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (!Enabled)
                return;
            if (_text != null && Font != null)
            {
                for (int i = 0; i < TextPos.Length; i++)
                    spriteBatch.DrawString(Font, Lines[i], TextPos[i], Color);
            }
            base.Draw(spriteBatch);
        }
    }

    public class Button : Control
    {
        private String _text;
        public String Text
        {
            get { return _text; }
            set
            {
                _text = value;
                if (Font != null && _text != null)
                {
                    Vector2 textSize = Font.MeasureString(_text);
                    TextPos = new Vector2(Position.Center.X - textSize.X / 2, Position.Center.Y - textSize.Y / 2);
                }
            }
        }
        private Vector2 TextPos = Vector2.Zero;
        void PosChanged(object sender, EventArgs e)
        {
            if (Font != null && _text != null)
            {
                Vector2 textSize = Font.MeasureString(_text);
                TextPos = new Vector2(Position.Center.X - textSize.X / 2, Position.Center.Y - textSize.Y / 2);
            }
        }
        
        public Color TextColor = Color.Black;
        /// <summary>
        /// Curent button state
        /// 0 - Default
        /// 1 - Clicked
        /// 2 - Hover
        /// </summary>
        public byte State;
        public Rectangle[] sourceRects;
        public Rectangle Source { get { return this.sourceRects[State]; } }
        public TileCollection TileCol;
        public SpriteFont Font;
        public bool Disabled = false;


        public Button(String name, Rectangle dest, TileCollection tileCollection, String text, SpriteFont font, EventHandler<EventArgs> onClick = null)
            : base(name, dest)
        {
            this.Font = font;
            this.Text = text;
            this.OnClick += onClick;
            this.TileCol = tileCollection;
            this.sourceRects = new Rectangle[3];
            UInt16 fGid = tileCollection.firstGID;
            for (int i = 0; i < 3; i++)
            {
                this.sourceRects[i] = tileCollection.GetSourceRect(fGid++);
                if (fGid >= tileCollection.elementCount)
                    fGid = tileCollection.firstGID;
            }
            base.PositionChanged += PosChanged;
        }
        internal override void InvokeClick()
        {
            if (!Disabled)
                base.InvokeClick();
        }
        public override void Update(GameTime gameTime)
        {
            if (Enabled && !Disabled)
            {
                MouseState mouseState = Mouse.GetState();
                if (Position.Contains(mouseState.X, mouseState.Y))
                {
                    if (Focused)
                    {
                        if (mouseState.LeftButton == ButtonState.Pressed)
                            State = 1;
                        else if (State == 1) // Click                        
                            State = 0;                        
                        else // hover                    
                            State = 2;
                    }
                    else // hover                
                        State = 2;
                }
                else if (State != 0)
                    State = 0;
            }
            base.Update(gameTime);
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (!Enabled)
                return;
            Color c = Color;
            if (Disabled)
                c = Color.Gray * TransitionAmt;
            spriteBatch.Draw(TileCol.texture, Position, Source, c);
            if (_text != null && Font != null)
            {
                Color txtC = TextColor * TransitionAmt;
                spriteBatch.DrawString(Font, _text, TextPos, txtC);
            }
            base.Draw(spriteBatch);
        }
    }
    public class ToggleButton : Control
    {
        private String _text;
        public String Text
        {
            get { return _text; }
            set
            {
                _text = value;
                if (Font != null && _text != null)
                {
                    Vector2 textSize = Font.MeasureString(_text);
                    TextPos = new Vector2(Position.Center.X - textSize.X / 2, Position.Center.Y - textSize.Y / 2);
                }
            }
        }
        private Vector2 TextPos = Vector2.Zero;
        void PosChanged(object sender, EventArgs e)
        {
            if (Font != null && _text != null)
            {
                Vector2 textSize = Font.MeasureString(_text);
                TextPos = new Vector2(Position.Center.X - textSize.X / 2, Position.Center.Y - textSize.Y / 2);
            }
        }
        bool changed;
        public Color TextColor = Color.Black;
        /// <summary>
        /// Curent button state
        /// 0 - Default
        /// 1 - Toggled
        /// 2 - Hover default
        /// 3 - Hover toggled
        /// </summary>
        public byte State;
        public Rectangle[] sourceRects;
        public Rectangle Source { get { return this.sourceRects[State]; } }
        public TileCollection TileCol;
        public SpriteFont Font;
        public bool Disabled = false;

        public ToggleButton(String name, Rectangle position, TileCollection tileCollection, String text, SpriteFont font, EventHandler<EventArgs> onClick = null)
            : base(name,position)
        {
            this.Font = font;
            this.Text = text;
            this.TileCol = tileCollection;
            this.sourceRects = new Rectangle[4];
            UInt16 fGid = tileCollection.firstGID;
            for (int i = 0; i < 4; i++)
            {
                this.sourceRects[i] = tileCollection.GetSourceRect(fGid++);
                if (fGid >= tileCollection.elementCount)
                    fGid = tileCollection.firstGID;
            }
            this.OnClick += onClick;
            base.PositionChanged += PosChanged;
        }
        internal override void InvokeClick()
        {
            if (!Disabled)
                base.InvokeClick();
        }
        public override void Update(GameTime gameTime)
        {
            if (Enabled && !Disabled)
            {
                MouseState mouseState = Mouse.GetState();
                if (Position.Contains(mouseState.X, mouseState.Y))
                {
                    if (Focused)
                    {
                        if (mouseState.LeftButton == ButtonState.Pressed)
                        {
                            if (!changed)
                            {
                                changed = true;
                                if (State == 1 || State == 3)
                                    State = 0;
                                else
                                    State = 1;
                            }
                        }
                        else // hover 
                        {
                            changed = false;
                            if (State == 0)
                                State = 2;
                            else if (State == 1)
                                State = 3;
                        }
                    }
                    else // hover                
                    {
                        if (State == 0)
                            State = 2;
                        else if (State == 1)
                            State = 3;
                    }
                } // end hover
                else if (State == 3)
                    State = 1;
                else if (State == 2)
                    State = 0;
            }
            base.Update(gameTime);
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (!Enabled)
                return;
            Color c = Color;
            if (Disabled)
                c = Color.Gray * TransitionAmt;
            spriteBatch.Draw(TileCol.texture, Position, Source, c);
            if (_text != null && Font != null)
            {
                Color txtC = TextColor * TransitionAmt;
                spriteBatch.DrawString(Font, _text, TextPos, txtC);
            }
            base.Draw(spriteBatch);
        }
    }

    public class FrameBox : Control
    {
        public TileCollection Collection;
        int borderW;
        int borderH;
        public int BorderW { get { return borderW; } set { borderW = value; } }
        public int BorderH { get { return borderH; } set { borderH = value; } } 
        public Rectangle InnerRect { get { return new Rectangle(Position.X + borderW, Position.Y + borderH, Position.Width - 2 * borderW, Position.Height - 2 * borderH); } }
        public FrameBox(String name, Rectangle dest, TileCollection texture)
            : base(name, dest)
        {
            this.Collection = texture;
            BorderW = texture.tileW;
            BorderH = texture.tileH;
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (!Enabled)
                return;

            bool topbottom = true;
            bool leftright = true;
            int w = Collection.tileW;
            int h = Collection.tileH;
            if (Position.Width <= h * 2)
            {
                w = Position.Width / 2;
                topbottom = false;
            }
            if (Position.Height <= h * 2)
            {
                h = Position.Height / 2;
                leftright = false;
            }
            spriteBatch.Draw(Collection.texture, new Rectangle(Position.X, Position.Y, w, h), Collection.GetSourceRect((UInt16)BorderTile.TopLeft), Color);
            spriteBatch.Draw(Collection.texture, new Rectangle(Position.Right - w, Position.Y, w, h), Collection.GetSourceRect((UInt16)BorderTile.TopRight), Color);
            spriteBatch.Draw(Collection.texture, new Rectangle(Position.X, Position.Bottom - h, w, h), Collection.GetSourceRect((UInt16)BorderTile.BottomLeft), Color);
            spriteBatch.Draw(Collection.texture, new Rectangle(Position.Right - w, Position.Bottom - h, w, h), Collection.GetSourceRect((UInt16)BorderTile.BottomRight), Color);
            if (topbottom)
            {
                int barW = Position.Width - w * 2;
                int tilesX = barW / Collection.tileW;
                int leftover = barW % Collection.tileW;
                for (int i = 0; i < tilesX; i++)
                {
                    spriteBatch.Draw(Collection.texture, new Rectangle(Position.X + w + i * Collection.tileW, Position.Y, Collection.tileW, h), Collection.GetSourceRect((UInt16)BorderTile.Top), Color);
                    spriteBatch.Draw(Collection.texture, new Rectangle(Position.X + w + i * Collection.tileW, Position.Bottom - h, Collection.tileW, h), Collection.GetSourceRect((UInt16)BorderTile.Bottom), Color);
                }
                if (leftover != 0)
                {
                    int posX = Position.X + w + tilesX * Collection.tileW;
                    int w2 = (Position.Right - w) -  posX;
                    spriteBatch.Draw(Collection.texture, new Rectangle(posX, Position.Y, w2, h), Collection.GetSourceRect((UInt16)BorderTile.Top), Color);
                    spriteBatch.Draw(Collection.texture, new Rectangle(posX, Position.Bottom - h, w2, h), Collection.GetSourceRect((UInt16)BorderTile.Bottom), Color);
                }               
            }
            if (leftright)
            {
                int barH = Position.Height - h * 2;
                int tilesY = barH / Collection.tileH;
                int leftover = barH % Collection.tileH;
                for (int i = 0; i < tilesY; i++)
                {
                    spriteBatch.Draw(Collection.texture, new Rectangle(Position.X, Position.Y + h + i * Collection.tileH, w, Collection.tileH), Collection.GetSourceRect((UInt16)BorderTile.Left), Color);
                    spriteBatch.Draw(Collection.texture, new Rectangle(Position.Right - w, Position.Y + h + i * Collection.tileH, w, Collection.tileH), Collection.GetSourceRect((UInt16)BorderTile.Right), Color);
                }
                if (leftover != 0)
                {
                    int posY = Position.Y + h + tilesY * Collection.tileH;
                    int h2 = Position.Bottom - h - posY;
                    spriteBatch.Draw(Collection.texture, new Rectangle(Position.X, posY, w, h2), Collection.GetSourceRect((UInt16)BorderTile.Left), Color);
                    spriteBatch.Draw(Collection.texture, new Rectangle(Position.Right - w, posY, w, h2), Collection.GetSourceRect((UInt16)BorderTile.Right), Color);
                }
            }
            if (topbottom && leftright)
                spriteBatch.Draw(Collection.texture, new Rectangle(Position.X + w, Position.Y + h, Position.Width - w * 2, Position.Height - h * 2), Collection.GetSourceRect((UInt16)BorderTile.Center), Color);


            base.Draw(spriteBatch);
        }
    }
    enum BorderTile
    {
        TopLeft = 0,
        Top = 1,
        TopRight = 2,
        Left = 3,
        Center = 4,
        Right = 5,
        BottomLeft = 6,
        Bottom = 7,
        BottomRight = 8
    }

    public class MessageBox : FrameBox
    {
        public event EventHandler<EventArgs> OnClose;
        private TextArea textArea;
        private TextArea titleText;
        public bool TitleBar;
        public string TitleText
        {
            get { return titleText.Text; }
            set { titleText.Text = value; }
        }
        private FrameBox titleBar;
        MouseState oldMouseState;
        public string Text
        {
            get { return textArea.Text; }
            set { textArea.Text = value; }
        }

        public MessageBox(String name, Rectangle position, TileCollection frame, TileCollection closeButton, String text, SpriteFont font)
            : base(name, position, frame)
        {
            titleBar = new FrameBox(String.Format("{0}_titlebar", Name), new Rectangle(position.X, position.Y - 28, position.Width, 32), frame);
            AddChild(titleBar);
            titleText = new TextArea(String.Format("{0}_titlebar_textarea", Name), new Rectangle(titleBar.Position.X, titleBar.Position.Y, titleBar.Position.Width - 28, titleBar.Position.Height), "", font);
            titleBar.AddChild(titleText);
            Button button = new Button(String.Format("{0}_titlebar_closebutton", Name), new Rectangle(position.Right - 28, position.Y - 22, 20, 20), closeButton, null, null, Close);
            titleBar.AddChild(button);
            textArea = new TextArea(String.Format("{0}_textarea", Name), new Rectangle(position.X + 10, position.Y + 10, position.Width - 20, position.Height - 20), text, font);
            AddChild(textArea);            
            oldMouseState = Mouse.GetState();
            
        }
        void Close(Object sender, EventArgs args)
        {
            if (OnClose != null)            
                OnClose(this, new HandledEventArgs());            
        }
        public override void Update(GameTime gameTime)
        {
            if (!Enabled)
                return;
            MouseState mouseState = Mouse.GetState();
            if (titleText.Focused && mouseState.LeftButton == ButtonState.Pressed)                          
                Position = new Rectangle(Position.X + mouseState.X - oldMouseState.X, Position.Y + mouseState.Y - oldMouseState.Y, Position.Width, Position.Height);            
            oldMouseState = mouseState;
            base.Update(gameTime);
        }
    }

    public class InputBox : Control
    {
        private String _text;
        public String Text
        {
            get { return _text; }
            set
            {
                _text = value;
                if (Font != null)
                {
                    Vector2 textSize = Font.MeasureString(_text);
                    if (textSize == Vector2.Zero)
                        textSize.Y = Font.MeasureString("|").Y;
                    TextPos = new Vector2(Position.Center.X - textSize.X / 2, Position.Center.Y - textSize.Y / 2);
                    CursorPos = new Vector2(Position.Center.X + textSize.X / 2, Position.Center.Y - textSize.Y / 2);
                }
            }
        }
        void PosChanged(object sender, EventArgs e)
        {
            if (Font != null)
            {
                Vector2 textSize = Font.MeasureString(_text);
                if (textSize == Vector2.Zero)
                    textSize.Y = Font.MeasureString("|").Y;
                TextPos = new Vector2(Position.Center.X - textSize.X / 2, Position.Center.Y - textSize.Y / 2);
                CursorPos = new Vector2(Position.Center.X + textSize.X / 2, Position.Center.Y - textSize.Y / 2);
            }
        }
        private Vector2 TextPos = Vector2.Zero;
        private Vector2 CursorPos = Vector2.Zero;
        public Color TextColor = Color.Black;
        public SpriteFont Font;

        public Texture2D Texture;
        public event EventHandler<EventArgs> OnType;
        public event EventHandler<EventArgs> OnEnter;
        private int CursorTime = 0;
        private bool Cursor;
        public int MaxLen;
        private Keys[] pressedKeys = new Keys[0];
        public bool AcceptNumbers = true;
        public bool AcceptLetters = true;
        public bool AcceptLowerCase = true;
        private bool IsShiftDown = true;
        private DateTime SinceLastDelete = DateTime.Now;

        public InputBox(String name, Rectangle dest, Texture2D texture, String text, SpriteFont font, int maxLenght = 0)
            : base(name, dest)
        {
            Font = font;
            Text = (text == null) ? "" : text;
            Texture = texture;
            MaxLen = maxLenght;
            base.PositionChanged += PosChanged;
        }
        public override void Update(GameTime gameTime)
        {
            if (!Enabled)
                return;

            if (Focused)
            {
                KeyboardState keyState = Keyboard.GetState();
                if (keyState.IsKeyDown(Keys.Enter))
                {
                    Focused = false;
                    if (OnEnter != null)
                        OnEnter(this, EventArgs.Empty);
                }
                else if (keyState.IsKeyDown(Keys.Back))
                {
                    if (Text.Length != 0 && (DateTime.Now - SinceLastDelete).TotalMilliseconds > 100)
                    {
                        Char[] chars = new Char[Text.Length - 1];
                        for (int c = 0; c < Text.Length - 1; c++)
                            chars[c] = Text[c];
                        Text = new String(chars);
                        SinceLastDelete = DateTime.Now;
                    }
                }
                else if (MaxLen == 0 || Text.Length < MaxLen)
                {
                    if (AcceptLowerCase && keyState.IsKeyUp(Keys.LeftShift) && keyState.IsKeyUp(Keys.RightShift))
                        IsShiftDown = false;
                    else
                        IsShiftDown = true;

                    Keys[] k = keyState.GetPressedKeys().Except(pressedKeys).ToArray();
                    for (int i = 0; i < k.Length; i++)
                    {
                       char c;
                       if (IsValid(k[i], out c))
                       {
                           string oldtext = Text;
                           Text += c;
                           if (OnType != null)
                           {
                               HandledEventArgs args = new HandledEventArgs();
                               OnType(this, args);
                               if (args.Handled)
                                   Text = oldtext;
                           }
                       }
                    }
                }
                CursorTime += gameTime.ElapsedGameTime.Milliseconds;
                if (CursorTime > 200)
                {
                    CursorTime -= 200;
                    Cursor = !Cursor;
                }
                pressedKeys = keyState.GetPressedKeys();
            }

            MouseState mouseState = Mouse.GetState();
            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                if (Position.Contains(mouseState.X, mouseState.Y))
                    Focused = true;
                else if (Focused)
                    Focused = false;
            }

            base.Update(gameTime);
        }
        bool IsValid(Keys k, out char c)
        {
            if (k == Keys.Space) { c = ' '; return true; }
            if (AcceptLetters)
            {
                string s = k.ToString();
                if (char.TryParse(s, out c))
                {
                    if (!IsShiftDown)
                        c = char.ToLower(c);
                    return true;
                }
            }
            if (AcceptNumbers)
            {
                if (k == Keys.D0 || k == Keys.NumPad0) { c = '0'; return true; }
                if (k == Keys.D1 || k == Keys.NumPad1) { c = '1'; return true; }
                if (k == Keys.D2 || k == Keys.NumPad2) { c = '2'; return true; }
                if (k == Keys.D3 || k == Keys.NumPad3) { c = '3'; return true; }
                if (k == Keys.D4 || k == Keys.NumPad4) { c = '4'; return true; }
                if (k == Keys.D5 || k == Keys.NumPad5) { c = '5'; return true; }
                if (k == Keys.D6 || k == Keys.NumPad6) { c = '6'; return true; }
                if (k == Keys.D7 || k == Keys.NumPad7) { c = '7'; return true; }
                if (k == Keys.D8 || k == Keys.NumPad8) { c = '8'; return true; }
                if (k == Keys.D9 || k == Keys.NumPad9) { c = '9'; return true; }
            }
            c = ' ';
            return false;
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (!Enabled)
                return;

            if (Texture != null)
                spriteBatch.Draw(Texture, Position, Color);

            Color txtC = TextColor;
            if (Transitioning)
                txtC *= TransitionAmt;
            spriteBatch.DrawString(Font, _text, TextPos, txtC);            
            if (Focused && Cursor)
                spriteBatch.DrawString(Font, "|", CursorPos, txtC);
            
            base.Draw(spriteBatch);
        }
    }

    public class Sprite : Control
    {
        public Texture2D Texture;
        public Rectangle? Source;
        public Sprite(String name, Rectangle position, Texture2D texture)
            : base(name, position)
        {
            Texture = texture;
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (!Enabled)
                return;
            spriteBatch.Draw(Texture, Position, Source, Color);
            base.Draw(spriteBatch);
        }
    }

    public class Slider : Control
    {
        public Texture2D Background;
        public Button Handle;
        public event EventHandler ValueChanged;
        bool grabbed;
        MouseState oldMouseState;
        public float Value
        {
            get
            {
                int xDif = Handle.Position.X - Position.X;
                return (float)Math.Round((float)xDif / (Position.Width - Handle.Position.Width), 3);
            }
            set
            {
                if (value < 0)
                    value = 0;
                if (value > 1)
                    value = 1;
                int xDif = (int)((Position.Width - Handle.Position.Width) * value);
                Handle.Position = new Rectangle(Position.X + xDif, Handle.Position.Y, Handle.Position.Width, Handle.Position.Height);
            }
        }

        public Slider(String name, Rectangle position, Texture2D background, TileCollection handle) : base (name, position)
        {
            Handle = new Button(String.Format("{0}_handle", name), new Rectangle(position.X, position.Y, (handle.tileW * position.Height) / handle.tileH, position.Height), handle, null, null, null);
            AddChild(Handle);
            Background = background;
            Handle.FocusChanged += OnHandleDefocus;
        }
        void OnHandleDefocus(object sender, EventArgs args)
        {
            if (!Handle.Focused)            
                grabbed = false;            
        }
        public override void Update(GameTime gameTime)
        {
            if (!Enabled)
                return;

            MouseState mouseState = Mouse.GetState();
            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                if (grabbed)
                {
                    int xDif = mouseState.X - oldMouseState.X;
                    if (xDif != 0)
                    {
                        int xNew = Handle.Position.X + xDif;
                        if (xNew >= Position.X && xNew <= Position.Right - Handle.Position.Width)
                        {
                            Handle.Position = new Rectangle(xNew, Handle.Position.Y, Handle.Position.Width, Handle.Position.Height);
                            ObjectEventArgs args = new ObjectEventArgs(Value);
                            if (ValueChanged != null)
                                ValueChanged(this, args);
                        }
                    }
                }
                else if (Handle.Focused)                
                    grabbed = true;                
            }
            else
                grabbed = false;

            oldMouseState = mouseState;

            base.Update(gameTime);
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (!Enabled)
                return;
            spriteBatch.Draw(Background, Position, Color);
            base.Draw(spriteBatch);
        }
    }
}
