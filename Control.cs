﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XNA_GUI_Controls
{
    /// <summary>
    /// Main Control class
    /// </summary>
    public class Control
    {
        public Object Tag;

        public bool Enabled = true;

        Rectangle position;
        /// <summary>
        /// Get and Set Control's position
        /// </summary>
        public Rectangle Position
        {
            get { return position; }
            set
            {
                ObjectEventArgs args = new ObjectEventArgs(position);
                position = value;
                if (PositionChanged != null)
                    PositionChanged(this, args);
                if (!args.Handled)
                {
                    Rectangle oldPos = (Rectangle)args.Tag;
                    float wDif = (float)position.Width / oldPos.Width;
                    float hDif = (float)position.Height / oldPos.Height;
                    lock (children)
                    {
                        foreach (Control c in children)
                        {
                            int nW, nH;
                            if (c.LockWidth)
                                nW = c.position.Width;
                            else
                                nW = (int)(c.position.Width * wDif);
                            if (c.LockHeight)
                                nH = c.position.Height;
                            else
                                nH = (int)(c.position.Height * hDif);

                            int nX = c.position.X;
                            int nY = c.position.Y;                            
                            if ((dock & 4) == 4) // docked left
                            {      
                                if ((dock & 8) == 8) // docked middle
                                    nX = c.position.Center.X + (position.Center.X - oldPos.Center.X) - nW / 2;
                                else
                                    nX = c.position.Left + (position.Left - oldPos.Left);                                        
                            }
                            else if ((c.dock & 8) == 8) // docked right
                                nX = c.position.Right + (position.Right - oldPos.Right);

                            if ((dock & 1) == 1) // docked top
                            {
                                if ((dock & 2) == 2) // docked middle
                                    nY = c.position.Center.Y + (position.Center.Y - oldPos.Center.Y) - nH / 2;
                                else
                                    nY = c.position.Top + (position.Top - oldPos.Top);
                            }
                            else if ((c.dock & 2) == 2) // docked bottom
                                nY = c.position.Bottom + (position.Bottom - oldPos.Bottom);                            
                            c.Position = new Rectangle(nX, nY, nW, nH);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Called when Position is changed
        /// </summary>
        public event EventHandler PositionChanged;


        internal virtual void InvokeClick()
        {
            if (OnClick != null)
                OnClick(this, EventArgs.Empty);
        }
        internal virtual void InvokeRClick()
        {
            if (OnRightClick != null)
                OnRightClick(this, EventArgs.Empty);
        }
        internal virtual void InvokeMouseOver()
        {
            if (OnMouseOver != null)
                OnMouseOver(this, EventArgs.Empty);
        }
        internal virtual void InvokeMouseLeave()
        {
            if (OnMouseLeave != null)
                OnMouseLeave(this, EventArgs.Empty);
        }
        public event EventHandler<EventArgs> OnClick;
        public event EventHandler<EventArgs> OnRightClick;
        public event EventHandler<EventArgs> OnMouseOver;
        public event EventHandler<EventArgs> OnMouseLeave;

        /// <summary>
        /// Lock the Width of the Control regardless of parent size changes.
        /// Default: false;
        /// </summary>
        public bool LockWidth = false;
        /// <summary>
        /// Lock the Height of the Control regardless of parent size changes.
        /// Default: false;
        /// </summary>
        public bool LockHeight = false;

        byte dock = 15;
        /// <summary>
        /// Dock bit flags. Can be combined.
        /// 0 = not docked
        /// 1 = top, 2 = bottom
        /// 4 = left, 8 = right
        /// 
        /// Default: 15
        /// </summary>
        public byte Dock
        {
            get { return dock; }
            set
            {
                byte valid = 1 | 2 | 4 | 8;
                if ((value & ~valid) == 0)                
                    dock = value;                
            }
        }
        

        /// <summary>
        /// Control's identifying name.
        /// </summary>
        public String Name;

        Control parent;
        /// <summary>
        /// Control's parent. 
        /// Null otherwise
        /// </summary>
        public Control Parent
        {
            get { return parent; }
            set
            {
                if (parent != value)
                {
                    if (parent != null)
                        parent.RemoveChild(this);
                    parent = value;
                    if (parent != null)
                        parent.AddChild(this);
                }
            }
        }


        /// <summary>
        /// Top Parent of the Control chain
        /// </summary>
        public Control TopParent
        {
            get
            {
                if (parent == null)
                    return this;
                else
                    return parent.TopParent;
            }
        }
        /// <summary>
        /// Array of child Controls.
        /// </summary>
        Control[] children = new Control[0];
        /// <summary>
        /// Get a Copy of the Children array.
        /// </summary>
        public Control[] Children { get { return children.ToArray(); } }
        /// <summary>
        /// Adds a child to the control and sets the child's Parent to this control
        /// </summary>
        /// <param name="c">child control</param>
        public void AddChild(Control c)
        {
            lock (children)
            {
                Control[] newChildren = new Control[children.Length + 1];
                for (int i = 0; i < children.Length; i++)
                {
                    newChildren[i] = children[i];
                    if (children[i] == c)
                        return;
                }
                newChildren[children.Length] = c;
                children = newChildren;
            }
            c.parent = this;
        }
        /// <summary>
        /// Removes a child
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public bool RemoveChild(Control c)
        {
            if (children.Length == 0)
                return false;
            lock (children)
            {
                Control[] newChildren = new Control[children.Length - 1];
                int index = 0;
                for (int i = 0; i < children.Length; i++)
                {
                    if (children[i] == c)
                        continue;
                    if (index == newChildren.Length)
                        return false;
                    newChildren[index++] = children[i];
                }
                children = newChildren;
                c.parent = null;
                return true;
            }
        }
        /// <summary>
        /// Remove all children
        /// </summary>
        public void ClearChildren()
        {
            lock (children)
            {
                foreach (Control c in children)
                    c.parent = null;
                children = new Control[0];
            }
        }
        /// <summary>
        /// Returns a Control at specified index in children array.
        /// Returns NULL if index is outside the bounds of the array.
        /// </summary>
        /// <param name="index">Position of control inside the children array</param>
        /// <returns>Control at specified index in children array.</returns>
        public Control GetChildAt(int index)
        {
            if (index >= 0 && index < children.Length)
                return children[index];
            else
                return null;
        }
        /// <summary>
        /// Find a child control by its name
        /// </summary>
        /// <param name="name">Identifying name of the control</param>
        /// <param name="recursive">Search children's children, etc.</param>
        /// <returns>Child Control or NULL</returns>
        public Control FindChild(string name, bool recursive = true)
        {
            lock (children)
            {
                foreach (Control c in children)
                {
                    if (c.Name == name)
                        return c;
                    else if (recursive)
                    {
                        Control ch = c.FindChild(name);
                        if (ch != null)
                            return ch;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Find a child controls by name
        /// </summary>
        /// <param name="name">Identifying name of the control</param>
        /// <param name="recursive">Search children's children, etc.</param>
        /// <returns>List of matching controls</returns>
        public List<Control> FindChildren(string name, bool recursive = true)
        {
            List<Control> returnList = new List<Control>();
            lock (children)
            {
                foreach (Control c in children)
                {
                    if (c.Name == name)
                        returnList.Add(c);
                    else if (recursive)                    
                        returnList.AddRange(c.FindChildren(name));                    
                }
            }
            return returnList;
        }

        /// <summary>
        /// Search recursively for Parent Control with specified Name
        /// </summary>
        /// <param name="name">Identifying name of the parent control</param>
        /// <returns>Parent Control or NULL</returns>
        public Control FindParent(string name)
        {
            Control p = parent;
            while (true)
            {
                if (p == null)
                    break;
                else if (p.Name == name)
                    return p;
                p = p.parent;
            }
            return null;
        }
        /// <summary>
        /// Get the top displayed Control amongst child Controls at the specified loaction.
        /// returns NULL if there are is no Control at the location;
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Control TopControlAt(int x, int y)
        {
            if (!Enabled)
                return null;
            for (int i = children.Length - 1; i >= 0; i--)
            {
                Control c = children[i].TopControlAt(x, y);
                if (c != null)
                    return c;
            }
            if (position.Contains(x, y))
                return this;
            return null;
        }
        public List<Control> ControlsAt(int x, int y)
        {
            List<Control> controls = new List<Control>();
            if (Enabled)
            {
                for (int i = children.Length - 1; i >= 0; i--)
                {
                    List<Control> c = children[i].ControlsAt(x, y);
                    if (c != null)
                        controls.AddRange(c);
                }
                if (position.Contains(x, y))
                    controls.Add(this);
            }
            return controls;
        }
        /// <summary>
        /// Bring the Control to the Front of the Parent's list.
        /// If recursive, bring the Parents to the top of their Parent's lists.
        /// </summary>
        public void BringToFront(bool recursive = false)
        {
            if (parent == null)
                return;
            lock (parent.children)
            {
                int lastID = parent.children.Length - 1;
                if (parent.children[lastID] != this)
                {
                    int insertIndex = 0;
                    for (int i = 0; i <= lastID; i++)
                        if (parent.children[i] != this)
                            parent.children[insertIndex++] = parent.children[i];
                    parent.children[lastID] = this;
                }
            }
            if (recursive)
                parent.BringToFront(true);
        }

        /// <summary>
        /// Get a Focused Control in this Control's chain
        /// </summary>
        /// <returns>Focused Control or NULL</returns>
        public Control GetFocused()
        {
            if (Focused)
                return this;
            lock (children)
            {
                foreach (Control c in children)
                {
                    Control f = c.GetFocused();
                    if (f != null)
                        return f;
                }
            }
            return null;
        }

        /// <summary>
        /// Called when the Control gains or looses Focus
        /// </summary>
        public event EventHandler FocusChanged;
        bool focused;
        /// <summary>
        /// Get or Set the Focus of the Control
        /// </summary>
        public bool Focused
        {
            get { return focused; }
            set
            {
                if (focused != value)
                {
                    if (value == true)
                    {
                        Control p = TopParent;
                        Control f = p.GetFocused();
                        if (f != null)                        
                            f.Focused = false;                        
                    }
                    focused = value;
                    if (FocusChanged != null)
                        FocusChanged(this, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Determines wheather any Parent Control until TopParent has Focused
        /// </summary>
        /// <returns></returns>
        public bool ParentsHaveFocus()
        {
            if (parent == null)
                return false;
            else if (parent.Focused)
                return true;
            else
                return parent.ParentsHaveFocus();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">Control's identifying name</param>
        /// <param name="position">Control's position</param>
        public Control(String name, Rectangle position)
        {
            this.Name = name;
            this.Position = position;
        }


        public bool Transitioning = false;
        bool fadeout;
        EventHandler FadedOut;
        public void FadeOut(int timeMS, bool recursive = true, EventHandler onFadeOut = null)
        {
            TransitionAmt = 1f;
            TransitionTime = timeMS;
            Transitioning = true;
            FadedOut += onFadeOut;
            if (recursive)
                foreach (Control c in children)
                    c.FadeOut(timeMS, recursive);
            fadeout = true;
        }
        bool fadein;
        public void FadeIn (int timeMS, bool recursive = true)
        {   
            TransitionAmt = 0f;
            TransitionTime = timeMS;
            Transitioning = true;
            if (recursive)
                foreach (Control c in children)
                    c.FadeIn(timeMS, recursive);
            fadein = true;            
        }

        public float TransitionAmt = 1f;
        public float TransitionTime = 300;
        Color ColorBase = Color.White;
        public Color Color
        {
            get
            {
                return ColorBase * TransitionAmt;
            }
            set { ColorBase = value; }
        }
        /// <summary>
        /// Update all the children of this control
        /// </summary>
        /// <param name="gameTime">current GameTime</param>
        public virtual void Update(GameTime gameTime)
        {
            if (!Enabled)
                return;
            if (Transitioning)
            {
                float dT = (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                if (fadeout)
                {
                    TransitionAmt -= (1 / TransitionTime) * dT;
                    if (TransitionAmt <= 0f)
                    {
                        Transitioning = false;
                        fadeout = false;
                        TransitionAmt = 0f;
                        if (FadedOut != null)
                            FadedOut(this, EventArgs.Empty);
                        FadedOut = null;
                    }
                }
                else
                {
                    TransitionAmt += (1 / TransitionTime) * dT;
                    if (TransitionAmt >= 1f)
                    {
                        Transitioning = false;
                        fadein = false;
                        TransitionAmt = 1f;
                    }
                }
            }
            lock (children)
            {
                for (int i = children.Length - 1; i >= 0; i--)
                {
                    try
                    {
                        children[i].Update(gameTime);
                    }
                    catch { }
                }
            }
        }
        /// <summary>
        /// Draw all the children of this control
        /// </summary>
        /// <param name="spriteBatch">initialized SpriteBatch</param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (!Enabled)
                return;
            lock (children)
            {
                foreach (Control c in children)
                    c.Draw(spriteBatch);
            }
        }
    }

    /// <summary>
    /// Subclass of HandledEventArgs with an Object Tag
    /// </summary>
    public class ObjectEventArgs : HandledEventArgs
    {
        public ObjectEventArgs(Object tag)
            : base()
        {
            Tag = tag;
        }
        public Object Tag;
    }
}
