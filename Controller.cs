﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace XNA_GUI_Controls
{
    /// <summary>
    /// Controller Control
    /// </summary>
    public class Controller : Control
    {
        TileCollection[] Frames;
        TileCollection[] Buttons;
        Texture2D[] Backgrounds;
        SpriteFont Font;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">Unique identifying name of the Control</param>
        /// <param name="size">Normaly size of the viewport. Can be empty rectangle.</param>
        /// <param name="font">SpriteFont to use with all text</param>
        public Controller(String name, Rectangle size, SpriteFont font) : base (name, size)
        {
            Frames = new TileCollection[0];
            Buttons = new TileCollection[0];
            Backgrounds = new Texture2D[0];
            Font = font;
            oldMS = Mouse.GetState();
        }

        /// <summary>
        /// Add texture style for Buttons to use.
        /// Texture tile order: Normal, Clicked, Hover
        /// </summary>
        /// <param name="texture">Texture containing button state tiles</param>
        /// <param name="tileW">Width of single button tile</param>
        /// <param name="tileH">Height of single button tile</param>
        /// <returns>Style index</returns>
        public int AddButtonStyle(Texture2D texture, ushort tileW, ushort tileH)
        {
            int newID = Buttons.Length;
            TileCollection[] newButtons = new TileCollection[newID + 1];
            for (int i = 0; i < newID; i++)
                newButtons[i] = Buttons[i];
            newButtons[newID] = new TileCollection(texture, tileW, tileH, 0);
            Buttons = newButtons;
            return newID;
        }
        /// <summary>
        /// Search Texture2D.Name tags for the matching Button style texture.
        /// Returns -1 if not found.
        /// </summary>
        /// <param name="textureName">Name of the texture</param>
        /// <returns>Style index</returns>
        public int FindButtonStyle(string textureName)
        {
            for (int i = 0; i < Buttons.Length; i++)
                if (Buttons[i].texture.Name == textureName)
                    return i;
            return -1;
        }

        /// <summary>
        /// Add texture style for FrameBox and MessageBox to use.
        /// Texure: 9 tiles representing the corners + center textures
        /// Texture tile order: top left, top, top right, left, center, right, bottom left, bottom, bottom right
        /// </summary>
        /// <param name="texture">Texture containing all frame tiles</param>
        /// <param name="tileW">Width of single tile</param>
        /// <param name="tileH">Height of single tile</param>
        /// <returns>Style index</returns>
        public int AddFrameStyle(Texture2D texture, ushort tileW, ushort tileH)
        {
            int newID = Frames.Length;
            TileCollection[] newFrames = new TileCollection[newID + 1];
            for (int i = 0; i < newID; i++)
                newFrames[i] = Frames[i];
            newFrames[newID] = new TileCollection(texture, tileW, tileH, 0);
            Frames = newFrames;
            return newID;
        }
        /// <summary>
        /// Search Texture2D.Name tags for the matching Frame style texture.
        /// Returns -1 if not found.
        /// </summary>
        /// <param name="textureName">Name of the texture</param>
        /// <returns>Style index</returns>
        public int FindFrameStyle(string textureName)
        {
            for (int i = 0; i < Frames.Length; i++)
                if (Frames[i].texture.Name == textureName)
                    return i;
            return -1;
        }
        /// <summary>
        /// Add texture style for InputBox to use.
        /// </summary>
        /// <param name="texture">Texture of InputBox background</param>
        /// <returns>Style index</returns>
        public int AddBackgroundStyle(Texture2D texture)
        {
            int newID = Backgrounds.Length;
            Texture2D[] newBackgrounds = new Texture2D[newID + 1];
            for (int i = 0; i < newID; i++)
                newBackgrounds[i] = Backgrounds[i];
            newBackgrounds[newID] = texture;
            Backgrounds = newBackgrounds;
            return newID;
        }
        /// <summary>
        /// Search Texture2D.Name tags for the matching Background style texture.
        /// Returns -1 if not found.
        /// </summary>
        /// <param name="textureName">Name of the texture</param>
        /// <returns>Style index</returns>
        public int FindBackgroundStyle(string textureName)
        {
            for (int i = 0; i < Backgrounds.Length; i++)
                if (Backgrounds[i].Name == textureName)
                    return i;
            return -1;
        }

        /// <summary>
        /// Adds a Button control to the list of controls.
        /// </summary>
        /// <param name="name">Identifying name of control</param>
        /// <param name="text">Text appearing on button.</param>
        /// <param name="position">Control's position</param>
        /// <param name="onClick">Called when button is clicked</param>
        /// <returns>Button Control</returns>
        /// <exception cref="ControlStyleNotInitialized">Thrown if no button style has been added</exception>
        public Button AddButton(string name, string text, Rectangle position, EventHandler<EventArgs> onClick) { return AddButton(name, text, position, onClick, 0, null); }
        /// <summary>
        /// Adds a Button control to the list of controls.
        /// </summary>
        /// <param name="name">Identifying name of control</param>
        /// <param name="text">Text appearing on button.</param>
        /// <param name="position">Control's position</param>
        /// <param name="onClick">Called when button is clicked</param>
        /// <param name="style">Style index</param>
        /// <returns>Button Control</returns>
        /// <exception cref="ControlStyleNotInitialized">Thrown if no button style has been added</exception>
        public Button AddButton(string name, string text, Rectangle position, EventHandler<EventArgs> onClick, int style) { return AddButton(name, text, position, onClick, style, null); }
        /// <summary>
        /// Adds a Button control to the list of controls.
        /// If Parent is NULL, the control is added to main list.
        /// Otherwise it is added to Parent's Children list of controls.
        /// </summary>
        /// <param name="name">Identifying name of control</param>
        /// <param name="text">Text appearing on button. Can be NULL</param>
        /// <param name="position">Control's position</param>
        /// <param name="onClick">Called when button is clicked</param>
        /// <param name="style">Style index</param>
        /// <param name="parent">Parent of this control</param>
        /// <returns>Button Control</returns>
        /// <exception cref="ControlStyleNotInitialized">Thrown if no button style has been added</exception>
        public Button AddButton(string name, string text, Rectangle position, EventHandler<EventArgs> onClick, int style, Control parent)
        {
            if (Buttons.Length == 0)
                throw new ControlStyleNotInitialized("Use AddButtonStyle() before adding a Button");
            if (style >= Buttons.Length || style < 0)
                style = 0;
            Button c = new Button(name, position, Buttons[style], text, Font, onClick);
            if (parent == null)
                this.AddChild(c);
            else
                parent.AddChild(c);
            return c;
        }

        /// <summary>
        /// Adds a ToggleButton control to the list of controls.
        /// </summary>
        /// <param name="name">Identifying name of control</param>
        /// <param name="text">Text on the button. Can be NULL</param>
        /// <param name="position">Control's position</param>
        /// <param name="onClick">Called when button is toggled</param>
        /// <param name="style">Style ID for the button</param>
        /// <returns></returns>
        public ToggleButton AddToggleButton(string name, string text, Rectangle position, EventHandler<EventArgs> onClick, int style) { return AddToggleButton(name, text, position, onClick, style, null); }
        /// <summary>
        /// Adds a ToggleButton control to the list of controls.
        /// If Parent is NULL, the control is added to main list.
        /// Otherwise it is added to Parent's Children list of controls.
        /// </summary>
        /// <param name="name">Identifying name of control</param>
        /// <param name="text">Text on the button. Can be NULL</param>
        /// <param name="position">Control's position</param>
        /// <param name="onClick">Called when button is toggled</param>
        /// <param name="style">Style ID for the button</param>
        /// <param name="parent">Parent of the button</param>
        /// <returns></returns>
        public ToggleButton AddToggleButton(string name, string text, Rectangle position, EventHandler<EventArgs> onClick, int style, Control parent)
        {
            if (Buttons.Length == 0)
                throw new ControlStyleNotInitialized("Use AddButtonStyle() before adding a Button");
            if (style >= Buttons.Length || style < 0)
                style = 0;
            ToggleButton c = new ToggleButton(name, position, Buttons[style], text, Font, onClick);
            if (parent == null)
                this.AddChild(c);
            else
                parent.AddChild(c);
            return c;
        }


        
        public FrameBox AddFrame(string name, Rectangle position) { return AddFrame(name, position, 0, null); }
        public FrameBox AddFrame(string name, Rectangle position, int style) { return AddFrame(name, position, style, null); }
        public FrameBox AddFrame(string name, Rectangle position, int style, Control parent)
        {
            if (Frames.Length == 0)
                throw new ControlStyleNotInitialized("Use AddFrameStyle() before adding a Frame");
            if (style >= Frames.Length || style < 0)
                style = 0;
            FrameBox c = new FrameBox(name, position, Frames[style]);
            if (parent == null)
                this.AddChild(c);
            else
                parent.AddChild(c);
            return c;
        }
        public MessageBox AddMsgBox(string name, Rectangle position, int frameStyle, int closeButtonStyle) { return AddMsgBox(name, position, frameStyle, closeButtonStyle, null, null); }
        public MessageBox AddMsgBox(string name, Rectangle position, int frameStyle, int closeButtonStyle, string text) { return AddMsgBox(name, position, frameStyle, closeButtonStyle, text, null); }
        public MessageBox AddMsgBox(string name, Rectangle position, int frameStyle, int closeButtonStyle, string text, Control parent)
        {
            if (Frames.Length == 0)
                throw new ControlStyleNotInitialized("Use AddFrameStyle() before adding a MessageBox");
            if (frameStyle >= Frames.Length || frameStyle < 0)
                frameStyle = 0;
            if (Buttons.Length == 0)
                throw new ControlStyleNotInitialized("Use AddButtonStyle() before adding a MessageBox");
            if (closeButtonStyle >= Buttons.Length || closeButtonStyle < 0)
                closeButtonStyle = 0;

            MessageBox c = new MessageBox(name, position, Frames[frameStyle], Buttons[closeButtonStyle], text, Font);
            c.OnClose += CloseMsgBox;
            if (parent == null)
                this.AddChild(c);
            else
                parent.AddChild(c);
            return c;
        }
        void CloseMsgBox(object sender, EventArgs args)
        {
            try
            {
                MessageBox mb = sender as MessageBox;
                Remove(mb, true, 330);
            }
            catch { }
        }
        
        public InputBox AddInput(string name, Rectangle position) { return AddInput(name, position, "", 0, 0, null); }
        public InputBox AddInput(string name, Rectangle position, string text, int maxLen, int style) { return AddInput(name, position, text, maxLen, style, null); }
        public InputBox AddInput(string name, Rectangle position, string text, int maxLen, int style, Control parent)
        {
            if (Backgrounds.Length == 0)
                throw new ControlStyleNotInitialized("Use AddBackgroundStyle() before adding an InputBox");
            if (style >= Backgrounds.Length || style < 0)
                style = 0;
            InputBox c = new InputBox(name, position, Backgrounds[style], text, Font, maxLen);
            if (parent == null)
                this.AddChild(c);
            else
                parent.AddChild(c);
            return c;
        }

        public TextArea AddTextArea(string name, string text, Rectangle position) { return AddTextArea(name, text, position, null); } 
        public TextArea AddTextArea(string name, string text, Rectangle position, Control parent)
        {
            TextArea c = new TextArea(name, position, text, Font);
            if (parent == null)
                this.AddChild(c);
            else
                parent.AddChild(c);
            return c;            
        }

        public Sprite AddSprite(string name, Rectangle position, Texture2D texture) { return AddSprite(name, position, texture, null); }
        public Sprite AddSprite(string name, Rectangle position, Texture2D texture, Control parent)
        {
            Sprite c = new Sprite(name, position, texture);
            if (parent == null)
                this.AddChild(c);
            else
                parent.AddChild(c);
            return c; 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="position"></param>
        /// <param name="backgroundStyle"></param>
        /// <param name="handleStyle"></param>
        /// <returns></returns>
        /// <exception cref="ControlStyleNotInitialized">Thrown if needed Style has not been initialized</exception>
        public Slider AddSlider(string name, Rectangle position, int backgroundStyle, int handleStyle) { return AddSlider(name, position, backgroundStyle, handleStyle, null); }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="position"></param>
        /// <param name="backgroundStyle"></param>
        /// <param name="handleStyle"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        /// <exception cref="ControlStyleNotInitialized">Thrown if needed Style has not been initialized</exception>
        public Slider AddSlider(string name, Rectangle position, int backgroundStyle, int handleStyle, Control parent)
        {
            if (Backgrounds.Length == 0)
                throw new ControlStyleNotInitialized("Use AddBackgroundStyle() before adding a Slider");
            if (Buttons.Length == 0)
                throw new ControlStyleNotInitialized("Use AddButtonStyle() before adding a Slider");
            if (backgroundStyle >= Backgrounds.Length || backgroundStyle < 0)
                backgroundStyle = 0;
            if (handleStyle >= Buttons.Length || handleStyle < 0)
                handleStyle = 0;
            Slider c = new Slider(name, position, Backgrounds[backgroundStyle], Buttons[handleStyle]);
            if (parent == null)
                this.AddChild(c);
            else
                parent.AddChild(c);
            return c; 
        }

        /// <summary>
        /// Remove the Control from Control's Parent list
        /// </summary>
        /// <param name="c">Control to remove</param>
        /// <returns></returns>
        public bool Remove(Control c) { return Remove(c, false, 0); }
        /// <summary>
        /// Remove the Control from the chain after fadeout
        /// </summary>
        /// <param name="c">Control to remove</param>
        /// <param name="fade">Is it fading out</param>
        /// <param name="timeMS">Fading time in miliseconds</param>
        /// <returns></returns>
        public bool Remove(Control c, bool fade, int timeMS)
        {
            if (c.Parent != null)
            {
                if (fade)
                {
                    c.FadeOut(timeMS, true, OnFadeOut);
                    return true;
                }
                else
                    return c.Parent.RemoveChild(c);
            }
            return false;
        }
        void OnFadeOut(Object sender, EventArgs args)
        {
            Control c = sender as Control;
            if (c != null)
                Remove(c);
        }
        /// <summary>
        /// Do controls fadein by default?
        /// Default: false
        /// </summary>
        public bool DefaultFadeIn = false;
        /// <summary>
        /// Default time in miliseconds for fadein.
        /// Default 330 ms;
        /// </summary>
        public int DefaultFadeTime = 330;

        Control oldTop;
        MouseState oldMS;
        /// <summary>
        /// Update all children and check if another control gained focus
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            // First update all children for the draggables to update themselves
            base.Update(gameTime);

            MouseState ms = Mouse.GetState();
            Control top = TopControlAt(ms.X, ms.Y);
            if (top != oldTop) // mouse over / out
            {
                if (oldTop != null)
                    oldTop.InvokeMouseLeave();
                if (top != null)
                    top.InvokeMouseOver();
            }
            if (top != null)
            {
                if (ms.LeftButton == ButtonState.Released && oldMS.LeftButton == ButtonState.Pressed) // end of click
                {
                    top.InvokeClick(); // invoke OnClick                    
                }
                else if (ms.LeftButton == ButtonState.Pressed && oldMS.LeftButton == ButtonState.Released) // beginning of click
                {
                    if (!top.Focused)// change of focus
                        top.Focused = true;
                }
                if (ms.RightButton == ButtonState.Pressed && oldMS.RightButton == ButtonState.Released) // right click
                {
                    top.InvokeRClick();
                }
            }

            oldMS = ms;
            oldTop = top;
        }
    }
    /// <summary>
    /// Indicates a certain Texture Style required for the Control has not been Added to the Controller
    /// </summary>
    [Serializable]
    public class ControlStyleNotInitialized : Exception
    {
        public ControlStyleNotInitialized(string message) : base(message) { }
    }
}
