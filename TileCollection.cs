﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XNA_GUI_Controls
{
    public class TileCollection
    {
        public Texture2D texture;
        public UInt16 tileW;
        public UInt16 tileH;
        /// <summary>
        /// Number of empty tiles at the end of the sheet
        /// </summary>
        public int PaddingCount;
        public int wTileCount { get { return texture.Width / tileW; } }
        public int hTileCount { get { return texture.Height / tileH; } }
        public int elementCount { get { return wTileCount * hTileCount - PaddingCount; } }
        public UInt16 firstGID;

        public TileCollection(Texture2D Texture, UInt16 TileW, UInt16 TileH, UInt16 FirstGID)
        {
            this.texture = Texture;
            this.tileW = TileW;
            this.tileH = TileH;
            this.firstGID = FirstGID;
        }
        public Rectangle GetSourceRect(UInt16 gID)
        {
            UInt16 setIndx = (UInt16)(gID - firstGID);
            return new Rectangle((setIndx % wTileCount) * tileW, (setIndx / wTileCount) * tileH, tileW, tileH);
        }
    }
}
